         CLIPS (6.31 6/12/19)
CLIPS> ;;; Automotive Expert System
;;;
;;; This expert system diagnoses some simple
;;; problems with a car and also recommends possible solutions to the problem.
;;; Developed: Igho
;;;
;;****************
;;* DEFINED FUNCTIONS *
;;****************
(deffunction ask-question (?question $?allowed-values)
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer)))
18
 (while (not (member ?answer ?allowed-values)) do
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer))))
 ?answer)
(deffunction yes-or-no-p (?question)
 (bind ?response (ask-question ?question yes no y n))
 (if (or (eq ?response yes) (eq ?response y))
 then yes
 else no))
19
;;;***************
;;;* QUERY LOGIC AND RULES *
;;;***************
(defrule determine-engine-state ""
 (not (engine-starts ?))
 (not (repair ?))
 =>
 (assert (engine-starts (yes-or-no-p "Does the Car start (yes/no)? "))))

(defrule determine-runs-normally ""
 (engine-starts yes)
20
 (not (repair ?))
 =>
 (assert (runs-normally (yes-or-no-p "Does the Car/Engine run normally (yes/no)? "))))
(defrule determine-rotation-state ""
 (engine-starts no)
 (not (repair ?))
 =>
 (assert (engine-rotates (yes-or-no-p "Does the Engine Blade Rotate (yes/no)? "))))

(defrule determine-sluggishness ""
 (runs-normally no)
21
 (not (repair ?))
 =>
 (assert (engine-sluggish (yes-or-no-p "Is the engine Sluggish or Slow to pick up (yes/no)?
"))))

(defrule determine-misfiring ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-misfires (yes-or-no-p "Does the Engine fail to Discharge or Misfire (yes/no)?
"))))
(defrule determine-knocking ""
 (runs-normally no)
22
 (not (repair ?))
 =>
 (assert (engine-knocks (yes-or-no-p "Does the Engine Knock i.e Combustion doesnt occur
(yes/no)? "))))
(defrule determine-low-output ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-output-low
 (yes-or-no-p "Is the Engine Transmission or Engine Power low (yes/no)? "))))
(defrule determine-gas-level ""
23
 (engine-starts no)
 (engine-rotates yes)
 (not (repair ?))
 =>
 (assert (tank-has-gas
 (yes-or-no-p "Is their Fuel in the Car (yes/no)? "))))
(defrule determine-battery-state ""
 (engine-rotates no)
 (not (repair ?))
 =>
 (assert (battery-has-charge
24
 (yes-or-no-p "Is the Car Battery Charged and in Good Working Condition (yes/no)?
"))))
(defrule determine-point-surface-state ""
 (or (and (engine-starts no)
 (engine-rotates yes))
 (engine-output-low yes))
 (not (repair ?))
 =>
 (assert (point-surface-state
 (ask-question "What is the surface state of the points (normal/burned/contaminated)? "
 normal burned contaminated))))
25
(defrule determine-conductivity-test ""
 (engine-starts no)
 (engine-rotates no)
 (battery-has-charge yes)
 (not (repair ?))
 =>
 (assert (conductivity-test-positive
 (yes-or-no-p "The Ignition Coil is working Properly (yes/no)? "))))
;;;****************
;;;CAR REPAIR LOGIC RULES
;;;****************
(defrule normal-engine-state-conclusions ""
 (runs-normally yes)
 (not (repair ?))
26
 =>
 (assert (repair "No repair needed.")))
(defrule engine-sluggish ""
 (engine-sluggish yes)
 (not (repair ?))
 =>
 (assert (repair "Clean the Fuel Pump.")))
(defrule engine-misfires ""
 (engine-misfires yes)
 (not (repair ?))
27
 =>
 (assert (repair "Point gap adjustment.")))
(defrule engine-knocks ""
 (engine-knocks yes)
 (not (repair ?))
 =>
 (assert (repair "Timing adjustment.")))
(defrule tank-out-of-gas ""
 (tank-has-gas no)
 (not (repair ?))
28
 =>
 (assert (repair "Add gas.")))
(defrule battery-dead ""
 (battery-has-charge no)
 (not (repair ?))
 =>
 (assert (repair "Charge the battery.")))
(defrule point-surface-state-burned ""
 (point-surface-state burned)
 (not (repair ?))
29
 =>
 (assert (repair "Replace the points.")))
(defrule point-surface-state-contaminated ""
 (point-surface-state contaminated)
 (not (repair ?))
 =>
 (assert (repair "Clean the points.")))
(defrule conductivity-test-positive-yes ""
 (conductivity-test-positive yes)
 (not (repair ?))
30
 =>
 (assert (repair "Repair the distributor lead wire.")))
(defrule conductivity-test-positive-no ""
 (conductivity-test-positive no)
 (not (repair ?))
 =>
 (assert (repair "Replace the ignition coil.")))
(defrule no-repairs ""
 (declare (salience -10))
 (not (repair ?))
31
 =>
 (assert (repair "Take your car to a mechanic.")))
;;;********************************
;;;* STARTUP AND CONCLUSION RULES *
;;;********************************
(defrule system-banner ""
 (declare (salience 10))
 =>
 (printout t crlf crlf)
 (printout t "The Car Engine Diagnosis Expert System
 Group 1 PROJECT
 COSC 882: Advanced Expert System and Contemporary Issues")
 (printout t crlf crlf))
32
(defrule print-repair ""
 (declare (salience 10))
 (repair ?item)
 =>
 (printout t crlf crlf)
 (printout t "Suggested Repair:")
 (printout t crlf crlf)
 (format t " %s%n%n%n" ?item))CLIPS> (run)
CLIPS> (run)
CLIPS> ;;; Automotive Expert System
;;;
;;; This expert system diagnoses some simple
;;; problems with a car and also recommends possible solutions to the problem.
;;; Developed: Igho
;;;
;;****************
;;* DEFINED FUNCTIONS *
;;****************
(deffunction ask-question (?question $?allowed-values)
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer)))
18
 (while (not (member ?answer ?allowed-values)) do
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer))))
 ?answer)
(deffunction yes-or-no-p (?question)
 (bind ?response (ask-question ?question yes no y n))
 (if (or (eq ?response yes) (eq ?response y))
 then yes
 else no))
19
;;;***************
;;;* QUERY LOGIC AND RULES *
;;;***************
(defrule determine-engine-state ""
 (not (engine-starts ?))
 (not (repair ?))
 =>
 (assert (engine-starts (yes-or-no-p "Does the Car start (yes/no)? "))))

(defrule determine-runs-normally ""
 (engine-starts yes)
20
 (not (repair ?))
 =>
 (assert (runs-normally (yes-or-no-p "Does the Car/Engine run normally (yes/no)? "))))
(defrule determine-rotation-state ""
 (engine-starts no)
 (not (repair ?))
 =>
 (assert (engine-rotates (yes-or-no-p "Does the Engine Blade Rotate (yes/no)? "))))

(defrule determine-sluggishness ""
 (runs-normally no)
21
 (not (repair ?))
 =>
 (assert (engine-sluggish (yes-or-no-p "Is the engine Sluggish or Slow to pick up (yes/no)?
"))))

(defrule determine-misfiring ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-misfires (yes-or-no-p "Does the Engine fail to Discharge or Misfire (yes/no)?
"))))
(defrule determine-knocking ""
 (runs-normally no)
22
 (not (repair ?))
 =>
 (assert (engine-knocks (yes-or-no-p "Does the Engine Knock i.e Combustion doesnt occur
(yes/no)? "))))
(defrule determine-low-output ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-output-low
 (yes-or-no-p "Is the Engine Transmission or Engine Power low (yes/no)? "))))
(defrule determine-gas-level ""
23
 (engine-starts no)
 (engine-rotates yes)
 (not (repair ?))
 =>
 (assert (tank-has-gas
 (yes-or-no-p "Is their Fuel in the Car (yes/no)? "))))
(defrule determine-battery-state ""
 (engine-rotates no)
 (not (repair ?))
 =>
 (assert (battery-has-charge
24
 (yes-or-no-p "Is the Car Battery Charged and in Good Working Condition (yes/no)?
"))))
(defrule determine-point-surface-state ""
 (or (and (engine-starts no)
 (engine-rotates yes))
 (engine-output-low yes))
 (not (repair ?))
 =>
 (assert (point-surface-state
 (ask-question "What is the surface state of the points (normal/burned/contaminated)? "
 normal burned contaminated))))
25
(defrule determine-conductivity-test ""
 (engine-starts no)
 (engine-rotates no)
 (battery-has-charge yes)
 (not (repair ?))
 =>
 (assert (conductivity-test-positive
 (yes-or-no-p "The Ignition Coil is working Properly (yes/no)? "))))
;;;****************
;;;CAR REPAIR LOGIC RULES
;;;****************
(defrule normal-engine-state-conclusions ""
 (runs-normally yes)
 (not (repair ?))
26
 =>
 (assert (repair "No repair needed.")))
(defrule engine-sluggish ""
 (engine-sluggish yes)
 (not (repair ?))
 =>
 (assert (repair "Clean the Fuel Pump.")))
(defrule engine-misfires ""
 (engine-misfires yes)
 (not (repair ?))
27
 =>
 (assert (repair "Point gap adjustment.")))
(defrule engine-knocks ""
 (engine-knocks yes)
 (not (repair ?))
 =>
 (assert (repair "Timing adjustment.")))
(defrule tank-out-of-gas ""
 (tank-has-gas no)
 (not (repair ?))
28
 =>
 (assert (repair "Add gas.")))
(defrule battery-dead ""
 (battery-has-charge no)
 (not (repair ?))
 =>
 (assert (repair "Charge the battery.")))
(defrule point-surface-state-burned ""
 (point-surface-state burned)
 (not (repair ?))
29
 =>
 (assert (repair "Replace the points.")))
(defrule point-surface-state-contaminated ""
 (point-surface-state contaminated)
 (not (repair ?))
 =>
 (assert (repair "Clean the points.")))
(defrule conductivity-test-positive-yes ""
 (conductivity-test-positive yes)
 (not (repair ?))
30
 =>
 (assert (repair "Repair the distributor lead wire.")))
(defrule conductivity-test-positive-no ""
 (conductivity-test-positive no)
 (not (repair ?))
 =>
 (assert (repair "Replace the ignition coil.")))
(defrule no-repairs ""
 (declare (salience -10))
 (not (repair ?))
31
 =>
 (assert (repair "Take your car to a mechanic.")))
;;;********************************
;;;* STARTUP AND CONCLUSION RULES *
;;;********************************
(defrule system-banner ""
 (declare (salience 10))
 =>
 (printout t crlf crlf)
 (printout t "The Car Engine Diagnosis Expert System
 Group 1 PROJECT
 COSC 882: Advanced Expert System and Contemporary Issues")
 (printout t crlf crlf))
32
(defrule print-repair ""
 (declare (salience 10))
 (repair ?item)
 =>
 (printout t crlf crlf)
 (printout t "Suggested Repair:")
 (printout t crlf crlf)CLIPS> (run)
CLIPS> (run)
CLIPS> ;;; Automotive Expert System
;;;
;;; This expert system diagnoses some simple
;;; problems with a car and also recommends possible solutions to the problem.
;;; Developed: Igho
;;;
;;****************
;;* DEFINED FUNCTIONS *
;;****************
(deffunction ask-question (?question $?allowed-values)
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer)))
18
 (while (not (member ?answer ?allowed-values)) do
 (printout t ?question)
 (bind ?answer (read))
 (if (lexemep ?answer)
 then (bind ?answer (lowcase ?answer))))
 ?answer)
(deffunction yes-or-no-p (?question)
 (bind ?response (ask-question ?question yes no y n))
 (if (or (eq ?response yes) (eq ?response y))
 then yes
 else no))
19
;;;***************
;;;* QUERY LOGIC AND RULES *
;;;***************
(defrule determine-engine-state ""
 (not (engine-starts ?))
 (not (repair ?))
 =>
 (assert (engine-starts (yes-or-no-p "Does the Car start (yes/no)? "))))

(defrule determine-runs-normally ""
 (engine-starts yes)
20
 (not (repair ?))
 =>
 (assert (runs-normally (yes-or-no-p "Does the Car/Engine run normally (yes/no)? "))))
(defrule determine-rotation-state ""
 (engine-starts no)
 (not (repair ?))
 =>
 (assert (engine-rotates (yes-or-no-p "Does the Engine Blade Rotate (yes/no)? "))))

(defrule determine-sluggishness ""
 (runs-normally no)
21
 (not (repair ?))
 =>
 (assert (engine-sluggish (yes-or-no-p "Is the engine Sluggish or Slow to pick up (yes/no)?
"))))

(defrule determine-misfiring ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-misfires (yes-or-no-p "Does the Engine fail to Discharge or Misfire (yes/no)?
"))))
(defrule determine-knocking ""
 (runs-normally no)
22
 (not (repair ?))
 =>
 (assert (engine-knocks (yes-or-no-p "Does the Engine Knock i.e Combustion doesnt occur
(yes/no)? "))))
(defrule determine-low-output ""
 (runs-normally no)
 (not (repair ?))
 =>
 (assert (engine-output-low
 (yes-or-no-p "Is the Engine Transmission or Engine Power low (yes/no)? "))))
(defrule determine-gas-level ""
23
 (engine-starts no)
 (engine-rotates yes)
 (not (repair ?))
 =>
 (assert (tank-has-gas
 (yes-or-no-p "Is their Fuel in the Car (yes/no)? "))))
(defrule determine-battery-state ""
 (engine-rotates no)
 (not (repair ?))
 =>
 (assert (battery-has-charge
24
 (yes-or-no-p "Is the Car Battery Charged and in Good Working Condition (yes/no)?
"))))
(defrule determine-point-surface-state ""
 (or (and (engine-starts no)
 (engine-rotates yes))
 (engine-output-low yes))
 (not (repair ?))
 =>
 (assert (point-surface-state
 (ask-question "What is the surface state of the points (normal/burned/contaminated)? "
 normal burned contaminated))))
25
(defrule determine-conductivity-test ""
 (engine-starts no)
 (engine-rotates no)
 (battery-has-charge yes)
 (not (repair ?))
 =>
 (assert (conductivity-test-positive
 (yes-or-no-p "The Ignition Coil is working Properly (yes/no)? "))))
;;;****************
;;;CAR REPAIR LOGIC RULES
;;;****************
(defrule normal-engine-state-conclusions ""
 (runs-normally yes)
 (not (repair ?))
26
 =>
 (assert (repair "No repair needed.")))
(defrule engine-sluggish ""
 (engine-sluggish yes)
 (not (repair ?))
 =>
 (assert (repair "Clean the Fuel Pump.")))
(defrule engine-misfires ""
 (engine-misfires yes)
 (not (repair ?))
27
 =>
 (assert (repair "Point gap adjustment.")))
(defrule engine-knocks ""
 (engine-knocks yes)
 (not (repair ?))
 =>
 (assert (repair "Timing adjustment.")))
(defrule tank-out-of-gas ""
 (tank-has-gas no)
 (not (repair ?))
28
 =>
 (assert (repair "Add gas.")))
(defrule battery-dead ""
 (battery-has-charge no)
 (not (repair ?))
 =>
 (assert (repair "Charge the battery.")))
(defrule point-surface-state-burned ""
 (point-surface-state burned)
 (not (repair ?))
29
 =>
 (assert (repair "Replace the points.")))
(defrule point-surface-state-contaminated ""
 (point-surface-state contaminated)
 (not (repair ?))
 =>
 (assert (repair "Clean the points.")))
(defrule conductivity-test-positive-yes ""
 (conductivity-test-positive yes)
 (not (repair ?))
30
 =>
 (assert (repair "Repair the distributor lead wire.")))
(defrule conductivity-test-positive-no ""
 (conductivity-test-positive no)
 (not (repair ?))
 =>
 (assert (repair "Replace the ignition coil.")))
(defrule no-repairs ""
 (declare (salience -10))
 (not (repair ?))
31
 =>
 (assert (repair "Take your car to a mechanic.")))
;;;********************************
;;;* STARTUP AND CONCLUSION RULES *
;;;********************************
(defrule system-banner ""
 (declare (salience 10))
 =>
 (printout t crlf crlf)
 (printout t "The Car Engine Diagnosis Expert System
 Group 1 PROJECT
 COSC 882: Advanced Expert System and Contemporary Issues")
 (printout t crlf crlf))
32
(defrule print-repair ""
 (declare (salience 10))
 (repair ?item)
 =>
 (printout t crlf crlf)
 (printout t "Suggested Repair:")CLIPS> 
